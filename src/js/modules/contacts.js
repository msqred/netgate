function ContactsPopup(wrap) {

    const contactBtn = wrap.querySelector('.contacts_btn');
    const contactPopup = wrap.querySelector('.contacts_popup');
    const contactCls = wrap.querySelector('.contacts_popup--cls');
    const contactBottom = wrap.querySelector('.contacts_bottom');
    const contactTop = wrap.querySelector('.contacts_top');
    const contactInput = wrap.querySelectorAll('.contacts_popup--input > input');

    console.log(contactInput);


    contactBtn.addEventListener('click', (e) => {
        e.preventDefault();
        contactPopup.classList.add('active');
        contactBottom.classList.add('hidden');
        contactTop.classList.add('hidden');
    });

    contactCls.addEventListener('click', (e) => {
        e.preventDefault();
        contactPopup.classList.remove('active');
        contactBottom.classList.remove('hidden');
        contactTop.classList.remove('hidden');
    });

    for (let i = 0; i < contactInput.length; i++) {
        contactInput[i].addEventListener('focus', (e) => {
            contactInput[i].parentNode.classList.add('click');
        });
        contactInput[i].addEventListener('blur', (e) => {
            contactInput[i].parentNode.classList.remove('click');
        });
        contactInput[i].addEventListener('change', (e) => {
            if (contactInput[i].value !== '') {
                contactInput[i].parentNode.classList.add('active');
            } else {
                contactInput[i].parentNode.classList.remove('active');
            }
        });
    }

}

export default ContactsPopup;