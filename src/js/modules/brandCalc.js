function brandCalc(wrap) {
    const btnType = wrap.querySelectorAll('.b_c--b--btn');
    const btnVariants = wrap.querySelectorAll('.b_c--q--btn');
    const resultPrice = wrap.querySelector('.b_c--right--result--sum span');
    const resultTime = wrap.querySelector('.b_c--right--result--day span');

    let value = 1, defaultPrice = btnType[0].childNodes[1].dataset.cost, cost = 300, time = 1;


    buttonsType(btnType);

    function buttonsType(arr) {
        for (let i = 0; i < arr.length; i++) {
            arr[i].addEventListener('click', (e) => {
                removeActive(arr);
                arr[i].classList.add('active');
                defaultPrice = arr[i].childNodes[1].dataset.cost;
                calculate(defaultPrice, value);
            })
        }
    }

    buttonsVariants(btnVariants);

    function buttonsVariants(arr) {
        for (let i = 0; i < arr.length; i++) {
            arr[i].addEventListener('click', (e) => {
                removeActive(arr);
                arr[i].classList.add('active');
                value = arr[i].childNodes[1].dataset.value;
                calculate(defaultPrice, value);
            })
        }
    }

    function removeActive(arr) {
        for (let i = 0; i < arr.length; i++) {
            arr[i].classList.remove('active');
        }
    }

    function calculate(btnType, btnVariants) {
        let oldCost = cost;
        cost = btnType * btnVariants;

        const timer = setInterval(() => {
            if (oldCost < cost) {
                resultPrice.innerText = `${oldCost = oldCost + 20}`;
                if (oldCost >= cost) {
                    clearInterval(timer);
                }
            } else {
                resultPrice.innerText = `${oldCost = oldCost - 20}`;
                if (oldCost <= cost) {
                    clearInterval(timer);
                }
            }
        }, 1);
    }

    defaultValue();

    function defaultValue() {
        resultPrice.innerHTML = `${defaultPrice}`;
        resultTime.innerHTML = `${time}`;
    }
}

export default brandCalc;