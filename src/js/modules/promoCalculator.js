function promoCalculator(wrap) {


    const minute = wrap.querySelector('[data-time="minute"]');
    const btnPlus = wrap.querySelector('.p_c--c--btn--plus');
    const btnMinus = wrap.querySelector('.p_c--c--btn--minus');
    const resultPrice = wrap.querySelector('.p_c--sum--price');
    const resultTime = wrap.querySelector('.p_c--sum--day');
    const buttonsArr = wrap.querySelectorAll('.p_c--c--btn');

    let value = 1, defaultPrice = buttonsArr[0].childNodes[1].dataset.cost, cost = 300, time = 0;


    function calculate(btnValue, timeValue) {
        let oldCost = cost;
        cost = btnValue * timeValue;

        const timer = setInterval(() => {
            if (oldCost < cost) {
                resultPrice.innerText = `$${oldCost = oldCost + 2}`;
                if (oldCost >= cost) {
                    clearInterval(timer);
                }
            } else {
                resultPrice.innerText = `$${oldCost = oldCost - 2}`;
                if (oldCost <= cost) {
                    clearInterval(timer);
                }
            }
        }, 1);
    }





    buttons(buttonsArr);

    function buttons(arr) {
        for (let i = 0; i < arr.length; i++) {
            arr[i].addEventListener('click', (e) => {
                removeActive(arr);
                arr[i].classList.add('active');
                defaultPrice = arr[i].childNodes[1].dataset.cost;
                calculate(defaultPrice, value);
            })
        }
    }

    function removeActive(arr) {
        for (let i = 0; i < arr.length; i++) {
            arr[i].classList.remove('active');
        }
    }


    defaultValue();

    function defaultValue() {
        resultPrice.innerHTML = `$${cost}`;
        resultTime.innerHTML = `${time} дней`;
    }


    plus(btnPlus, minute);
    minus(btnMinus, minute);

    function plus(element, item) {
        element.addEventListener('click', (e) => {
            value = item.innerHTML;
            value++;
            item.innerHTML = value;
            calculate(defaultPrice, value);
        })
    }

    function minus(element, item) {
        element.addEventListener('click', (e) => {
            if (value > 1) {
                value = item.innerHTML;
                value--;
                item.innerHTML = value;
                calculate(defaultPrice, value);
            } else {
                value = 1;
            }
        })
    }
}

export default promoCalculator;