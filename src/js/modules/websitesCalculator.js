function websitesCalculator(wrap) {

    const arrAll = document.querySelectorAll('input');
    const btnOrder = document.querySelector('.btn_order--websites');
    const popup = document.querySelector('.popup_confirm');
    const popupCls = document.querySelector('.popup_cls');

    const checkBoxArr = wrap.querySelectorAll('.w_m--m-line--check > input');
    const inputArr = wrap.querySelectorAll('.w_m--cost-block > input[name="number"]');
    const resultPrice = document.querySelector('.w_b--top--result--cost');
    const resultPricePopup = document.querySelector('.popup_site--sum--cost span');
    const checkPopup = document.querySelector('.popup_site--type');

    /*Top variables Type sections*/
    const typeWrap = wrap.querySelector('.w_m--type');
    const typeNameInner = document.querySelector('.w_b--type span');
    const typeCostInner = document.querySelector('.w_b--type--cost');
    const typeCheckArr = typeWrap.querySelectorAll('input');
    /*Top variables Design sections*/
    const designWrap = wrap.querySelector('.w_m--design');
    const designNameInner = document.querySelector('.w_b--design span');
    const designCostInner = document.querySelector('.w_b--design--cost');
    const designCheckArr = designWrap.querySelectorAll('input');
    /*Top variables Design sections*/
    const adaptiveWrap = wrap.querySelector('.w_m--adaptive');
    const adaptiveNameInner = document.querySelector('.w_b--adaptive span');
    const adaptiveCostInner = document.querySelector('.w_b--adaptive--cost');
    const adaptiveCheckArr = adaptiveWrap.querySelectorAll('input');

    let parent, name, cost, adaptiveName = [], adaptiveCost = [], adaptiveCostSum = 0, oldCost = 0, newCost = 0, sum;
    let allData = {
        type: '0',
        design: '0',
        adaptive: '0',
        napolnenie: '0'
    };


    function sumObj() {
        oldCost = 0;
        oldCost = newCost;
        sum = 0;
        for (let key in allData) {
            sum = sum + parseFloat(allData[key]);
        }
        newCost = sum;
        animateCalc(oldCost, newCost);
    }


    function animateCalc(oldCost, newCost) {

        const timer = setInterval(() => {
            if (oldCost < newCost) {
                resultPrice.innerText = `$${oldCost = oldCost + 10}`;
                resultPricePopup.innerText = `${newCost}`;
                if (oldCost === cost) {
                    clearInterval(timer);
                }
            } else if (oldCost > newCost) {
                resultPrice.innerText = `$${oldCost = oldCost - 10}`;
                resultPricePopup.innerText = `${newCost}`;
                if (oldCost === newCost) {
                    clearInterval(timer);
                }
            }
        }, 5);
    }


    /*Set info to Result section of Type*/
    setName(typeCheckArr, typeNameInner);
    setCost(typeCheckArr, typeCostInner, 'type');

    /*Set info to Result section of Design*/
    setName(designCheckArr, designNameInner);
    setCost(designCheckArr, designCostInner, 'design');

    /*Set info to Result section of Adaptive*/
    setNameAdaptive(adaptiveCheckArr, adaptiveNameInner);
    setCostAdaptive(adaptiveCheckArr, adaptiveCostInner, 'adaptive');

    function setNameAdaptive(arr, to) {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i].checked) {
                let adName = arr[i].nextElementSibling.getAttribute('data-text');
                adaptiveName.push(adName);
                to.innerText = `${adaptiveName[0]}`;
            }
            arr[i].addEventListener('change', (e) => {
                if (arr[i].checked) {
                    let adName = arr[i].nextElementSibling.getAttribute('data-text');
                    adaptiveName.push(adName);
                    to.innerText = `${adaptiveName[0]} + ${adaptiveName[1]}`;
                } else {
                    adaptiveName.splice(1);
                    to.innerText = `${adaptiveName[0]}`;
                }
            });
        }
    }

    function setCostAdaptive(arr, to, index) {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i].checked) {
                let adCost = arr[i].nextElementSibling.getAttribute('data-cost');
                adaptiveCost.push(adCost);
                for (let p = 0; p < adaptiveCost.length; p++) {
                    adaptiveCostSum = adaptiveCostSum + parseInt(adaptiveCost[p]);
                }
                to.innerText = `${adaptiveCostSum} USD`;
                allData[index] = adaptiveCostSum;
                sumObj();
            }
            arr[i].addEventListener('change', (e) => {
                if (arr[i].checked) {
                    let adCost = arr[i].nextElementSibling.getAttribute('data-cost');
                    adaptiveCost.push(adCost);
                    adaptiveCostSum = 0;
                    for (let p = 0; p < adaptiveCost.length; p++) {
                        adaptiveCostSum = adaptiveCostSum + parseInt(adaptiveCost[p]);
                    }
                    to.innerText = `${adaptiveCostSum} USD`;
                    allData[index] = adaptiveCostSum;
                    sumObj();
                } else {
                    adaptiveCostSum = 0;
                    adaptiveCost.splice(1);
                    for (let p = 0; p < adaptiveCost.length; p++) {
                        adaptiveCostSum = adaptiveCostSum + parseInt(adaptiveCost[p]);
                    }
                    to.innerText = `${adaptiveCostSum} USD`;
                    allData[index] = adaptiveCostSum;
                    sumObj();
                }
            })
        }
    }


    function setName(arr, to) {
        for (let i = 0; i < arr.length; i++) {
            arr[i].addEventListener('change', (e) => {
                if (arr[i].checked) {
                    name = arr[i].nextElementSibling.getAttribute('data-text');
                    to.innerText = name;
                    if (checkPopup !== null) {
                        checkPopup.innerText = name;
                    }
                }
            })
        }
    }

    function setCost(arr, to, index) {
        for (let i = 0; i < arr.length; i++) {
            arr[i].addEventListener('change', (e) => {
                if (arr[i].checked) {
                    cost = arr[i].nextElementSibling.getAttribute('data-cost');
                    to.innerText = `${cost} USD`;
                    allData[index] = parseInt(cost);
                    sumObj();
                }
            })
        }
    }


    addActiveInputs(checkBoxArr);

    function addActiveInputs(arr) {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i].checked) {
                arr[i].parentNode.parentNode.classList.toggle('active');
            }
            arr[i].addEventListener('click', (e) => {
                parent = arr[i].parentNode.parentNode;
                parent.classList.toggle('active');
                if (parent.querySelector('.w_m--cost-block > input[name="number"]') && parent.querySelector('.w_m--cost-block > input[name="number"]').value <= 0 && parent.classList.contains('active')) {
                    parent.querySelector('.w_m--cost-block > input[name="number"]').value = 1;
                }
                if (parent.querySelector('.w_m--cost-block > input[name="number"]') && parent.querySelector('.w_m--cost-block > input[name="number"]').value !== 0 && !parent.classList.contains('active')) {
                    parent.querySelector('.w_m--cost-block > input[name="number"]').value = 0;
                }
            })
        }
    }

    inputValue(inputArr);


    function inputValue(arr) {
        for (let i = 0; i < arr.length; i++) {
            arr[i].addEventListener('change', (e) => {
                if (arr[i].value > 0) {
                    parent = arr[i].parentNode.parentNode;
                    parent.classList.add('active');
                } else {
                    parent.classList.remove('active');
                }
            })
        }
    }


    /*popup*/
    btnOrder.addEventListener('click', (e) => {
        e.preventDefault();
        popup.classList.add('active');
    });

    popupCls.addEventListener('click', (e) => {
        e.preventDefault();
        popup.classList.remove('active');
    });

}

export default websitesCalculator;