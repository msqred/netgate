function Feedback(arr) {

    const $ = require("jquery");

    const popupFeedback = document.querySelector('.contacts_popup');
    let url = '/';

    for (let i = 0; i < arr.length; i++) {

        arr[i].addEventListener('submit', function (event) {
            event.preventDefault();
            let formData = new FormData(arr[i]);
            let action = arr[i].getAttribute('action');
            formData.append('action', action);
            $.ajax({
                url: `${url}`,
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false
            });

            popupFeedback.classList.add('success');

            /*setTimeout(() => {
                popupFeedback.classList.remove('success');
                popupFeedback.classList.remove('active');
            }, 3500);*/
            console.log(formData);
        })

    }



}

export default Feedback;

