function preloader(wrap, callback) {
    setTimeout(() => {
        wrap.classList.add('load');
    }, 50);
    setTimeout(() => {
        wrap.classList.remove('active');
        callback();
    }, 1700);
}


export default preloader;