function Language(elem) {
    elem.addEventListener('click', (e) => {
        elem.classList.toggle('active');
    });
}

export default Language;