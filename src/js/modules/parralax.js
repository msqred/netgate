function Parallax(arr) {
    const arrParallax = arr;
    document.addEventListener('mousemove', function (e) {
        let _w = window.innerWidth / 2;
        let _h = window.innerHeight / 2;
        let _mouseX = e.clientX;
        let _mouseY = e.clientY;
        let depth1 = `${50 - (_mouseX - _w) * 0.02}`;
        let depth2 = `${50 - (_mouseY - _h) * 0.01}`;
        for (let i = 0; i < arrParallax.length; i++) {
            arrParallax[i].style.transform = 'translate(' + -depth2 + 'px, ' + -depth1 + 'px) scale(1.2)';
        }
    });

}

export default Parallax;