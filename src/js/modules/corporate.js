function CorporateItems(arr) {
    for (let i = 0; i < arr.length; i++) {
        arr[i].addEventListener('click', (e) => {
            e.preventDefault();
            removeActive(arr);
            arr[i].classList.toggle('active');
        })
    }
}

function removeActive(arr) {
    for (let i = 0; i < arr.length; i++) {
        arr[i].classList.remove('active');
    }
}

export default CorporateItems;