/*Scroll Reveal*/
import ScrollReveal from 'scrollreveal';


function scrollReveal() {

    /*animation up*/
    ScrollReveal().reveal('.up', {
        distance: '100px',
        interval: 400,
        duration: 1700,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1)',
        delay: 500,
        opacity: 0
    });

    /*animation up*/
    ScrollReveal().reveal('.down', {
        distance: '100px',
        origin: 'top',
        interval: 400,
        duration: 1700,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1)',
        delay: 500,
        opacity: 0
    });


    ScrollReveal().reveal('.scale', {
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1)',
        duration: 3000,
        scale: .1
    });

    /*animation up*/
    ScrollReveal().reveal('.up_o', {
        distance: '100px',
        interval: 400,
        duration: 1700,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1)',
        opacity: 1
    });


    ScrollReveal().reveal('.similar_hotel--item', {
        distance: '100px',
        interval: 400,
        duration: 1700,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1.275)'
    });

    ScrollReveal().reveal('.header_up', {
        distance: '40px',
        interval: 400,
        duration: 1700,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1.275)'
    });

    ScrollReveal().reveal('.search_title', {
        distance: '100px',
        interval: 400,
        duration: 1700,
        delay: 10,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1)'
    });

    ScrollReveal().reveal('.filter_tour--item', {
        distance: '100px',
        interval: 400,
        duration: 1700,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1)'
    });

    ScrollReveal().reveal('.title_block', {
        distance: '100px',
        interval: 400,
        duration: 1700,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1.275)'
    });

    ScrollReveal().reveal('.work_with--img', {
        distance: '100px',
        interval: 400,
        duration: 1700,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1.275)'
    });

    ScrollReveal().reveal('.hot_slider', {
        distance: '100px',
        interval: 1500,
        duration: 1700,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1.275)'
    });


    ScrollReveal().reveal('.similar_hotel--slider', {
        distance: '100px',
        interval: 400,
        duration: 1700,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1.275)'
    });

    ScrollReveal().reveal('.hot_item--animation', {
        distance: '100px',
        interval: 400,
        duration: 1700,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1.275)'
    });



    /*show x*/
    ScrollReveal().reveal('.show_left', {
        distance: '-200px',
        origin: 'right',
        interval: 400,
        duration: 1700,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1)'
    });

    ScrollReveal().reveal('.show_right', {
        distance: '200px',
        origin: 'right',
        interval: 400,
        duration: 1700,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1)'
    });

    ScrollReveal().reveal('.description', {
        distance: '0',
        interval: 2100,
        duration: 2000,
        opacity: 0
    });

    ScrollReveal().reveal('.interesting_item', {
        distance: '0',
        interval: 400,
        duration: 1700,
        opacity: 0
    });

    ScrollReveal().reveal('.about_do--item', {
        distance: '0',
        interval: 400,
        duration: 1700,
        opacity: 0
    });

    ScrollReveal().reveal('.alternate_slider--wrap', {
        distance: '0',
        interval: 400,
        duration: 1700,
        opacity: 0
    });

    ScrollReveal().reveal('.category_chill--top_item', {
        distance: '100px',
        interval: 400,
        duration: 1700,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1.275)'
    });

    ScrollReveal().reveal('.question--item', {
        distance: '100px',
        interval: 400,
        duration: 1700,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1.275)'
    });

    ScrollReveal().reveal('.about_find--img', {
        distance: '100px',
        interval: 400,
        duration: 1700,
        easing: 'cubic-bezier(0.175, 0.885, 0.32, 1.275)'
    });

}

export default scrollReveal;