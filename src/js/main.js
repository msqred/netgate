'use strict';


/*
*
* Variables
*
* */
/*preloader*/
const preloader = document.querySelector('.preloader');
/*Menu Burger*/
const menuBurger = document.querySelector('.m_s--nav');
const mainMenu = document.querySelector('.main_menu');
/*Right menu lang*/
const menuLang = document.querySelector('.m_s--lang');
/*animation*/
const animationLoad = document.querySelectorAll('.animation');
/*Corporate items arr*/
const corporateArr = document.querySelectorAll('.c_p--g--item');
/*Contacts page*/
const contactPage = document.querySelector('.contacts_page');
/*contact form*/
const forms = document.querySelectorAll('.feedback');
/*promo Calc*/
const promoCalc = document.querySelector('.p_c--wrap-calculator');
/*websites Calc*/
const websitesCalc = document.querySelector('.websites_m');
/*swup*/
const SwupArr = document.querySelectorAll('#swup');
/*toCalc*/
const toCalc = document.querySelectorAll('.toCalc');
const calc = document.querySelectorAll('#calc');

/*brand calc*/
const brandCalculator = document.querySelector('.b_c--calculator');


/*
*
* Imports
*
* */

/*Burger*/
import Burger from './modules/burger';
/*Lang*/
import Language from "./modules/language";
/*Corporate Items*/
import CorporateItems from "./modules/corporate";
/*Contacts popup*/
import ContactsPopup from './modules/contacts';
/*animations*/
import animations from './modules/scrollreveal';
/*feedback*/
import Feedback from './modules/feedback';
/*promo Calc*/
import promoCalculator from "./modules/promoCalculator";
/*websites Calc*/
import websitesCalculator from "./modules/websitesCalculator";
/*animation*/
import animationScroll from './modules/animation';
/*brand calc*/
import brandCalc from "./modules/brandCalc";
/*scroll to block*/
import {scrollToBlock} from './modules/scrollTo';
import preloaderLoad from './modules/preloader';



/*
*
* Require function
*
* */



/*Burger*/
if (menuBurger !== null && mainMenu !== null) {
    Burger(menuBurger, mainMenu);
}

/*Lang*/
if (menuLang !== null) {
    Language(menuLang);
}

/*Corporate Items*/
if (corporateArr.length > 0) {
    CorporateItems(corporateArr);
}

/*Contacts popup*/
if (contactPage !== null) {
    ContactsPopup(contactPage);
}

/*feedback*/
if (forms.length > 0) {
    Feedback(forms);
}

/*promo calc*/
if (promoCalc !== null) {
    promoCalculator(promoCalc);
}

if (websitesCalc !== null) {
    websitesCalculator(websitesCalc);
}

if (brandCalculator !== null) {
    brandCalc(brandCalculator);
}

if (animationLoad.length > 0) {
    animationScroll(animationLoad);

    window.addEventListener('scroll', () => {
        animationScroll(animationLoad);
    });
}

/*go To Calc*/
if (toCalc.length > 0 && calc.length > 0) {
    for (let i = 0; i < toCalc.length; i++) {
        scrollToBlock(calc[i], toCalc[i]);
    }
}


/*preloader*/
if (preloader !== null) {
    preloaderLoad(preloader, execution);
}

function execution() {
    animations();
}
